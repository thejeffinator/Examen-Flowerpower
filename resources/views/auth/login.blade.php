@extends('layout.master')

@section('main_content')
    <div class="content">
        <h1>Medewerker login</h1>
        <form method="POST" action="/auth/login">
            {!! csrf_field() !!}

            <div class="row">
                <button type="submit" class="btn limegreen-button">Login</button>
            </div>
            @if (count($errors) > 0)
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif
            <div class="row">
                <label>Gebruikersnaam</label><br>
                <input type="text" name="gebruikersnaam" value="{{ old('gebruikersnaam') }}">
            </div>

            <div class="row">
                <label>Password</label><br>
                <input type="password" name="password" id="password">
            </div>

            <div class="row">
                <button type="submit" class="btn limegreen-button">Login</button>
            </div>
        </form>
    </div>
@stop