@extends('layout.master')

@section('main_content')
    <div class="content">

        <div class="row">
            <div class="col-md-4">
                <img src="/assets/images/almere.jpg" class="front-img">
                <h1>FlowerPower</h1>
                FlowerPower staat voor klantvriendelijkheid en levering van de best mogelijke kwaliteitsproducten.
                U kunt bij ons kiezen uit een breed en gevarieerd assortiment aan bloemen en planten. FlowerPower is gevestigd in Apeldoorn,
                Den Bosch, Almere en Zutphen. Kom gerust eens een kijkje nemen in
                één van de winkels. Ook kunt u op deze website bloemen bestellen en afhalen op een moment dat u goed uitkomt. Tot snel!
            </div>
            <div class="col-md-8">


                <img src="/assets/images/boeket2.png" class="front-img">
            </div>
        </div>

        <div class="row">

        </div>
    </div>

@stop
