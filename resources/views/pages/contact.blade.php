@extends('layout.master')

@section('main_content')
    <div class="content">
        <div class="row">
            <h1>Neem contact op met Flowerpower</h1>
            <div class="col-md-3">
                <img src="/assets/images/almere.jpg" class="winkel">
                <p>
                    <h4>Flowerpower Almere</h4>
                    <span>Almerestraat 81</span><br />
                    <span>1520 JS Almere</span><br />
                    <span>+316 12345678</span><br />
                    <span><a href="mailto:almere@flowerpower.nl">almere@flowerpower.nl</a></span>
                </p>
            </div>
            <div class="col-md-3">
                <img src="/assets/images/apeldoorn.jpg" class="winkel">
                <p>
                    <h4>Flowerpower Apeldoorn</h4>
                    <span>Apeldoornlaan 1</span><br />
                    <span>4920 KP Apeldoorn</span><br />
                    <span>+319 29301934</span><br />
                    <span><a href="mailto:apeldoorn@flowerpower.nl">apeldoorn@flowerpower.nl</a></span>
                </p>
            </div>
            <div class="col-md-3">
                <img src="/assets/images/denbosch.jpg" class="winkel">
                <p>
                    <h4>Flowerpower Den Bosch</h4>
                    <span>Den Boschweg 99</span><br />
                    <span>9019 PA Den Bosch</span><br />
                    <span>+318 92830482</span><br />
                    <span><a href="mailto:denbosch@flowerpower.nl">denbosch@flowerpower.nl</a></span>
                </p>
            </div>
            <div class="col-md-3">
                <img src="/assets/images/zutphen.jpg" class="winkel">
                <p>
                    <h4>Flowerpower Zutphen</h4>
                    <span>Zutphenlaan 290</span><br />
                    <span>3001 EP Zutphen</span><br />
                    <span>+302 74382910</span><br />
                    <span><a href="mailto:zutphen@flowerpower.nl">zutphen@flowerpower.nl</a></span>
                </p>
            </div>
        </div>
    </div>

@stop
