<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>FlowerPower</title>

    <!-- Bootstrap CSS served from a CDN -->
    <link rel='stylesheet' href="/assets/css/bootstrap.min.css" >
    <link rel='stylesheet' href="/assets/style.css" >

</head>

<body>
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">

                <a class="navbar-brand" href="/"><img src="/assets/images/logo.png"/></a>


        </div>
        <div>
            <span class="navbar-flowerpower">Flowerpower<span>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

            <ul class="nav navbar-nav navbar-right">
                @if(Session::get('user_id'))
                    <li><a><span>Ingelogd als {{Session::get('user_id')}}</span></a></li>
                    <li><a href="/auth/logout">Logout</a></li>
                @else
                    <li><a class="btn" href="/medewerkers">Login als medewerker</a></li>
                @endif


            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>

<?php
    /**
     * Include Main content Yield for the content
     */
?>
        <div class="menu">
            <ul>
                <li><a href="#">Artikelen</a></li>
                <li><a href="/medewerkers">Medewerkers</a></li>
                <li><a href="/contact">Contact</a></li>
                @if(Session::get('user_id'))
                <li><a href="/bestellingen">Bestellingen</a></li>
                @endif
            </ul>
        </div>
        <div class="content" >
            @yield('main_content')
        </div>


<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
<script src="/assets/js/bootstrap.min.js"></script>
</body>
</html>
