@extends('layout.master')

@section('main_content')
    <?php
        header( "refresh:5;url=/" );
    ?>
    <div class="content">
        <h1>Niet ingelogd</h1>
        <h4>Login om deze pagina te kunnen zien</h4>
        <p>U wordt binnen 5 seconden terug gestuurd naar de homepagina</p>
    </div>

@stop
