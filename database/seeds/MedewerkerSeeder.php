<?php

use Illuminate\Database\Seeder;

class MedewerkerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('medewerker')->insert([
            'gebruikersnaam' => 'test1',
            'mederwerkercode' => 'test002',
            'wachtwoord' => Hash::make('test001'),
        ]);

    }
}

